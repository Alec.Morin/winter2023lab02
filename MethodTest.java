public class MethodTest{
	
	public static void main(String[]args){
		SecondClass help = new  SecondClass();
		String s1 = "java";
		String s2 = "programming";
		int w = s1.length();
		int h = s2.length();
		System.out.println(w +" "+ h);
		int x = 5;
		System.out.println("The first value is " + x);
		methodNoInputNoReturn();
		methodOneInputNoReturn(x + 10);
		System.out.println("The final value is " + x);
		methodTwoInputNoReturn(7, 20);
		int z = methodNoInputReturnInt();
		System.out.println(z);
		double root = sumSquareRoot(9, 5);
		System.out.println("Square root is " + root);
		int s = SecondClass.addOne(50);
		int p = help.addTwo(50);
		System.out.println(s + " " + p);
	}
	
	
	public static void methodNoInputNoReturn(){
		int x = 20;
		System.out.println("The method's x value is " + x);
		System.out.println("I’m in a method that takes no input and returns nothing");
	}
	
	public static void methodOneInputNoReturn(int y){
		y = y - 5;
		System.out.println("Inside the method one input no return " + y);
	}
	
	public static void  methodTwoInputNoReturn(int z, double b){
		System.out.println("The input value is " + z + " and the double value is " + b);
	}
	
	public static int methodNoInputReturnInt(){
		return 5;
	}
	
	public static double sumSquareRoot(int n1, int n2){
		double total = n1 + n2;
		return total = Math.sqrt(total);
	}
}