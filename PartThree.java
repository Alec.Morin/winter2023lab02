import java.util.Scanner;

public class PartThree{
	
	public static void main(String[]args){
		Scanner scan = new Scanner(System.in);
		Calculator cal = new Calculator();
		
		System.out.println("Please enter two number:");
		double x = scan.nextDouble();
		double y = scan.nextDouble();
		
		double sum = Calculator.add(x, y);
		System.out.println(sum);
		
		double sub = Calculator.subtract(x, y);
		System.out.println(sub);
		
		double mult = cal.multiply(x, y);
		System.out.println(mult);
		
		double div = cal.divide(x, y);
		System.out.println(div);
	}
}