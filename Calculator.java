public class Calculator{
	public static double add(double x, double y){
		double sum = x + y;
		return sum;
	}
	
	public static double subtract(double x, double y){
		double sub = x - y;
		return sub;
	}
	
	public double multiply(double x, double y){
		double prod = x * y;
		return prod;
	}
	
	public double divide(double x, double y){
		double div = x / y;
		return div;
	}
}